# nbdt-social
An interview task built with
typescript, grpc, level-ts
### Getting Started
 
#### Install & Run

- ```yarn install```<br/>
Install dependencies<br/>
- ```yarn run generate-proto```<br/>
Generate proto files to ./generated <br/>
- ```yarn run build-ts```<br/>
Compile  js from ts to ./dist <br/>
- ```node run ./dist/grpc-server.js```<br/>
Run grpc server at "localhost:50051" <br/>

### API
#### Types
- ##### Post <br/>
Contains post data (message,tags,id)
- ##### QueryPost<br/>
(limit,offset)
- ##### Result<br/>
Contains method call result (success,postId)
- ##### PostEvent <br/>
Post events streamed from server <br/> 
type (enum for deleted,created,edited,result)<br/>
post (affected post data  *other field except id can be null)

  


#### PostService
- ##### CreatePost (Post) returns (Result) <br/>
Emits created event and saves Post to db
- ##### EditPost  (Post) returns (Result)
- ##### DeletePost Post) returns (Result)
- ##### GetPosts (stream QueryPost) returns (stream PostEvent)<br/>
Creates a stream than you can send QueryPost objects and recieve PostEvents
