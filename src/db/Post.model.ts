import {ulid} from "ulid"
import Level from "level-ts";

export class PostModel {
    private static _instance: PostModel;
    private _db: Level;

    private constructor() {
        if (!PostModel._instance) {
            this._db = new Level("./data")
        }

    }

    public async put(value) {
        let key = ulid();
        value.id = key;
        //let validObj =  new PostSchema(value.toObject());
        //validObj.validate()
        //*TODO Error while validating schema  TypeError: Cannot read property 'name' of undefined
        return this._db.put(key, value)
            .then(() => {
                return key
            })
    }

    public async delete(postId: string) {
        return this._db.del(postId).then(() => {
            return postId
        })
    }

    public async edit(data) {
        if (data.id !== null) {
            throw new Error("PostId can not be null")
        }
        return this.delete(data.id)
            .then(() => {
                return this._db.put(data.id, data)
                    .then(() => {
                        return data.id
                    })
            })
    }

    public async get(limit: number, offset: number, cb) {
        var dbstr = await this._db.stream({reverse: true, limit: limit + offset})
        for (var i = 0; i < dbstr.length; i++) {
            if (i > offset - 1) {
                {
                    cb(dbstr[i])
                }
            }
        }
    }

    public static get Instance() {
        return this._instance || (this._instance = new this());
    }

}