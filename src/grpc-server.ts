import * as grpc from "grpc";
import * as PostServicegrpc from "../generated/PostService_grpc_pb";
import * as PostService from "../generated/PostService_pb";
import {PostEvent} from "../generated/PostService_pb";
import {PostModel} from "./db/Post.model"
import {EventEmitter} from "events";

let connections = [];
const _events = new EventEmitter();
function createPost(call, cb) {
    let reslt = new PostService.Result();
    PostModel.Instance.put(call.request.toObject())
        .then(key => {
            call.request.setId(key);
            emitEvent(PostEvent.EventType.CREATED, call.request);
            reslt.setSuccess(true);
            reslt.setPostid(key);
            cb(null, reslt)
        })
        .catch(cb)
}

function sendEvents2grpc() {
    _events.on("event", function (eventData) {
        connections.forEach(_call => {
            _call.write(eventData)
        })
    })
}

sendEvents2grpc();
function editPost(call, cb) {
    let reslt = new PostService.Result();
    PostModel.Instance.edit(call.request.toObject())
        .then((id) => {
            emitEvent(PostEvent.EventType.EDITED, call.request);
            reslt.setSuccess(true);
            reslt.setPostid(id);
            cb(null, reslt)
        })
        .catch(cb)
}

function deletePost(call, cb) {
    let reslt = new PostService.Result();
    PostModel.Instance.delete(call.request.toObject().postid)
        .then((id) => {
            emitEvent(PostEvent.EventType.DELETED, call.request);
            reslt.setSuccess(true);
            reslt.setPostid(id);
            cb(null, reslt)
        })
        .catch(cb)
}
function getPosts(call) {
    connections.push(call);
    call.on("data", function (data) {
        let queryObj = data.toObject();

        PostModel.Instance.get(queryObj.limit, queryObj.offset,
            function (data) {
                let _obj = data.value;
                let _postEvent = new PostService.PostEvent();
                _postEvent.setType(PostEvent.EventType.RESULT);
                let _post = new PostService.Post();
                _postEvent.setPost(_post);
                _post.setId(_obj.id);
                _post.setTagsList(_obj.tagsList);
                _post.setMessage(_obj.message);
                call.write(_postEvent)
            })
    });
    call.on("end", function (sth) {
        let i = connections.indexOf(call);
        connections.splice(i, 1)
    })
}

function emitEvent(type: PostService.PostEvent.EventType, post: PostService.Post) {
    var _event = new PostService.PostEvent();
    _event.setType(type);
    _event.setPost(post);
    _events.emit("event", _event)
}
function startServe() {
    let server = new grpc.Server();
    server.addService(PostServicegrpc.PostServiceService,
        {createPost, getPosts, editPost, deletePost});
    server.bind(
        'localhost:50051', grpc.ServerCredentials.createInsecure());
    server.start();
}

startServe();
